# frozen_string_literal: true

require "nuku/extractor"


module Nuku::Extractors
  # @note Only used internally for testing
  class CardTitleExtractor < Nuku::Extractor
    set_option yields_value: true

    procedure do
      first_by!(css: ".card-title").content!
    end
  end


  # @note Only used internally for testing
  class TestExtractor < Nuku::Extractor
    set_option wrap: true

    procedure do
      capture :card_titles do
        find_by!(css: ".card") do |ct|
          CardTitleExtractor.extract(ct)
        end
      end

      capture :second do
        first_by!(css: "section > article").content!
      end
    end
  end
end
