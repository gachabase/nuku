# frozen_string_literal: true

require "active_support/core_ext/hash/deep_merge"


module Nuku
  module Errors
    class CaptureError < NukuError
    end
  end


  class Capture
    DEFAULT_SCOPE_OPTIONS = {
      replace: false,
      allow_empty: false,
      allow_nil: false
    }.freeze

    ALLOWED_SCOPE_OPTIONS = %i[
      replace
      allow_empty
      allow_nil
      default
    ].freeze

    def initialize
      clear
    end

    def scope(name, **options)
      enter_scope(name, options)
      value = yield
      update_memo(value)
      nil
    ensure
      exit_scope
    end

    def scope_options(*path)
      @scope_options[path.map(&:to_sym).hash]
    end

    def clear
      @memo = {}
      @path = []
      @scope_options = {}
    end

    def to_h
      build_hash
    end

  private

    def enter_scope(name, **options)
      path = @path.push(name.to_sym).dup
      options = options.select { |k, _| ALLOWED_SCOPE_OPTIONS.include?(k) }
      @scope_options[path.hash] ||= DEFAULT_SCOPE_OPTIONS.dup
      @scope_options[path.hash].update(options)
      path
    end

    def exit_scope
      @path.pop
    end

    def build_nested(keys, acc = {})
      return acc if keys.empty?
      build_nested(keys[0..-2], { keys.last => acc })
    end

    def build_hash
      @memo.each_with_object({}) do |(path, value), acc|
        acc.deep_merge!(build_nested(path[0..-2], { path.last => value }))
      end
    end

    def join_values(head, tail)
      tail.is_a?(Array) ? [head, *tail] : [head, tail]
    end

    def options
      @scope_options[@path.hash]
    end

    def value_valid?(value)
      case value
      when Array, Hash then options[:allow_empty] || !value.empty?
      when nil         then options[:allow_nil]
      else true
      end
    end

    def scope_exists?
      @memo.key?(@path)
    end

    def current_scope
      @memo[@path.dup]
    end

    def set_scope(val)
      @memo[@path.dup] = val
    end

    def prepare_value(value)
      if value_valid?(value)
        value
      elsif !scope_exists? && options.key?(:default)
        options[:default]
      else
        throw :skip_value
      end
    end

    def update_scope(value)
      case current_scope
      when Array
        value.is_a?(Array) ? set_scope(current_scope + value) : current_scope << value
      when Hash
        if value.is_a?(Hash)
          current_scope.update(value)
        else
          set_scope(join_values(current_scope, value))
        end
      else
        set_scope(join_values(current_scope, value))
      end
    end

    def update_memo(value)
      catch(:skip_value) do
        value = prepare_value(value)

        if scope_exists? && !options[:replace] && current_scope != options[:default]
          update_scope(value)
        else
          set_scope(value)
        end
      end
    end
  end
end
